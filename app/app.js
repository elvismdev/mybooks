'use strict';

// Declare app level module which depends on views, and components
angular.module('myBooks', [
    'ngRoute',
    'myBooks.view1',
    'myBooks.view2',
    'myBooks.version'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.otherwise({
        redirectTo: '/view1'
    });
}]);
